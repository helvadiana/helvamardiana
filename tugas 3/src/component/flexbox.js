import React, {Component} from 'react';
import {View, StyleSheet, Text, Image, ScrollView,} from 'react-native';


class FlexBox extends Component {
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.boxOne}>
                    <Text style={styles.teks}>Instagram</Text>
                    <Image source={require('/Users/Diana/diana/src/assets/upu.jpg')} style={styles.like}/>
                </View>
                <View style={styles.boxThree}>

                    <ScrollView>
                    <Image source={require('/Users/Diana/diana/src/assets/cerita.jpg')} style={styles.home}/>
                    <Image source={require('/Users/Diana/diana/src/assets/isi.jpg')} style={styles.isi}/>
                    <Image source={require('/Users/Diana/diana/src/assets/isi.jpg')} style={styles.isi}/>
                    <Image source={require('/Users/Diana/diana/src/assets/isi.jpg')} style={styles.isi}/>
                    <Image source={require('/Users/Diana/diana/src/assets/isi.jpg')} style={styles.isi}/>
                    <Image source={require('/Users/Diana/diana/src/assets/isi.jpg')} style={styles.isi}/>
                    <Image source={require('/Users/Diana/diana/src/assets/isi.jpg')} style={styles.isi}/>
                    <Image source={require('/Users/Diana/diana/src/assets/isi.jpg')} style={styles.isi}/>
                    <Image source={require('/Users/Diana/diana/src/assets/isi.jpg')} style={styles.isi}/>
                    <Image source={require('/Users/Diana/diana/src/assets/isi.jpg')} style={styles.isi}/>
                    <Image source={require('/Users/Diana/diana/src/assets/isi.jpg')} style={styles.isi}/>
                    </ScrollView>
                </View>
                <View style={styles.boxFour}>
                    <Image source={require('/Users/Diana/diana/src/assets/home.jpg')} style={styles.bawah}/>
                    <Image source={require('/Users/Diana/diana/src/assets/cari.jpg')} style={styles.bawah}/>
                    <Image source={require('/Users/Diana/diana/src/assets/plus.jpg')} style={styles.bawah}/>
                    <Image source={require('/Users/Diana/diana/src/assets/shop.jpg')} style={styles.bawah}/>
                    <Image source={require('/Users/Diana/diana/src/assets/profil.jpg')} style={styles.bawah}/>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection:'column'
    },
    boxOne: {
        flex: 1,
        alignItems:'center',
        justifyContent:'space-between',
        flexDirection: 'row',
        backgroundColor:'white'
    },
    boxThree: {
        flex: 9
    },
    boxFour: {
        flex: 1,
        flexDirection:'row',
        backgroundColor:'white',
        justifyContent:'space-around'
    },
    teks: {
        fontSize: 30
    },
    dosa: {
        width: 70,
        height: 70,
        borderRadius: 20
        
    },
    like: {
        width: 90,
        height: 50
    },
    home: {
        width: 350,
        height: 90
    },
    isi: {
        width:355,
        height:600
    },
    bawah: {
        width: 60,
        height: 60
    }
});

export default FlexBox;